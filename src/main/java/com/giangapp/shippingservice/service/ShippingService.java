package com.giangapp.shippingservice.service;

import com.giangapp.shippingservice.domain.Shipping;

import java.util.List;

public interface ShippingService {
    List<Shipping> getAll();
}
