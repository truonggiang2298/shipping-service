package com.giangapp.shippingservice.service.impl;

import com.giangapp.shippingservice.domain.Shipping;
import com.giangapp.shippingservice.repository.ShippingRepository;
import com.giangapp.shippingservice.service.ShippingService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ShippingServiceImpl implements ShippingService {
    private final ShippingRepository shippingRepository;
    @Override
    public List<Shipping> getAll() {
        return shippingRepository.findAll();
    }
}
