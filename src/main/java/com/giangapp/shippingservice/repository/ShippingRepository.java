package com.giangapp.shippingservice.repository;

import com.giangapp.shippingservice.domain.Shipping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingRepository  extends JpaRepository<Shipping,Long> {
}
