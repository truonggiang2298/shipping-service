package com.giangapp.shippingservice.controller;

import com.giangapp.shippingservice.domain.Shipping;
import com.giangapp.shippingservice.service.ShippingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ShippingController {
    private final ShippingService shippingService;
    @RequestMapping(path =  "/getAll",method = RequestMethod.GET)
    public List<Shipping> getAllShip(){
        return shippingService.getAll();
    }
    @RequestMapping(path="/hello",method = RequestMethod.GET)
    public String hello(){
        return "hello";
    }
}
